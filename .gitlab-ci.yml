stages:
- build
- build-image
- review
- staging
- production
- cleanup

.auto-deploy:
  image: registry.gitlab.com/gitlab-org/cluster-integration/auto-deploy-image:v0.9.0

include:
- project: open/gitlab-ci
  file: /kaniko-packager.yml

variables:
  AUTO_DEPLOY_IMAGE: registry.gitlab.com/gitlab-org/cluster-integration/auto-deploy-image:v0.9.0
  ROLLOUT_RESOURCE_TYPE: deploy
  CI_APPLICATION_REPOSITORY: ${CI_REGISTRY_IMAGE}
  CI_APPLICATION_TAG: ${CI_PIPELINE_ID}

build-site:
  stage: build
  image: docker.io/library/node:10.15.3
  artifacts:
    name: site
    paths:
    - build
  cache:
    key: v1-docusaurus-node-modules
    paths:
    - node_modules
  script:
  - yarn install
  - yarn run build

build-image:
  stage: build-image
  extends:
  - .kaniko-packager
  cache:
    key: v1-docusaurus-kaniko-cache
    paths:
    - $CI_PROJECT_DIR/.kaniko_cache
  dependencies:
  - build-site
  variables:
    KANIKO_CONTEXT_DIR: .
    KANIKO_DEBUG_IMAGE: 'gcr.io/kaniko-project/executor:debug-v0.16.0'
    KANIKO_DESTINATION: $CI_REGISTRY_IMAGE:$CI_PIPELINE_ID
    KANIKO_EXTRA_ARGS: --cache=true --cache-dir=$CI_PROJECT_DIR/.kaniko_cache

review:
  extends: .auto-deploy
  stage: review
  script:
  - auto-deploy check_kube_domain
  - auto-deploy download_chart
  - auto-deploy ensure_namespace
  - auto-deploy initialize_tiller
  - auto-deploy create_secret
  - auto-deploy deploy
  - auto-deploy persist_environment_url
  environment:
    name: &review_name review/$CI_COMMIT_REF_NAME
    url: https://$CI_ENVIRONMENT_SLUG.$KUBE_INGRESS_BASE_DOMAIN
    on_stop: stop_review
  artifacts:
    paths: [environment_url.txt]
  only:
    refs:
    - branches
    - tags
    kubernetes: active
  except:
    refs:
    - master
    variables:
    - $REVIEW_DISABLED

staging:
  extends: .auto-deploy
  stage: staging
  script:
  - auto-deploy check_kube_domain
  - auto-deploy download_chart
  - auto-deploy ensure_namespace
  - auto-deploy initialize_tiller
  - auto-deploy create_secret
  - auto-deploy deploy
  environment:
    name: staging
    url: https://docs.staging.openraven.com
  only:
    refs:
    - master
    kubernetes: active
    variables:
    - $STAGING_ENABLED

production:
  extends: .auto-deploy
  stage: production
  script:
  - auto-deploy check_kube_domain
  - auto-deploy download_chart
  - auto-deploy ensure_namespace
  - auto-deploy initialize_tiller
  - auto-deploy create_secret
  - auto-deploy deploy
  - auto-deploy delete canary
  - auto-deploy delete rollout
  - auto-deploy persist_environment_url
  environment:
    name: production
    url: https://docs.openraven.com
  except:
    variables:
    - $STAGING_ENABLED
  artifacts:
    paths: [environment_url.txt]
  only:
    refs:
    - master
    kubernetes: active

stop_review:
  extends: .auto-deploy
  stage: cleanup
  variables:
    GIT_STRATEGY: none
  script:
  - auto-deploy initialize_tiller
  - auto-deploy delete
  environment:
    name: *review_name
    action: stop
  when: manual
  allow_failure: true
  only:
    refs:
    - branches
    - tags
    kubernetes: active
  except:
    refs:
    - master
    variables:
    - $REVIEW_DISABLED
