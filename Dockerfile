FROM docker.io/library/nginx:1.17.8
COPY build /usr/share/nginx/html/docs
COPY redirect_to_docs.html /usr/share/nginx/html/index.html
RUN ls -l  /usr/share/nginx/html /usr/share/nginx/html/docs
