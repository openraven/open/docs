const path = require('path');

module.exports = {
  title: 'Open Raven Documentation',
  tagline: 'Open Raven Documentation',
  url: 'https://your-docusaurus-test-site.com',
  baseUrl: '/docs/',
  favicon: 'img/favicon.ico',
  organizationName: 'Open Raven', // Usually your GitHub org/user name.
  projectName: 'docusaurus', // Usually your repo name.
  themeConfig: {
    disableDarkMode: true,
    prism: {
      theme: require('prism-react-renderer/themes/vsDark'),
    },
    algolia: {
      apiKey: '47ecd3b21be71c5822571b9f59e52544',
      indexName: 'docusaurus-2',
    },
    navbar: {
      title: 'Open Raven Docs',
      logo: {
        alt: 'My Site Logo',
        src: 'img/logo.svg',
      },
      links: [
        {to: 'docs/doc1', label: 'Docs', position: 'left'},
      ],
    },
    footer: {
    },
    googleAnalytics: {
      trackingID: 'UA-143777557-2',
    }
  },
  presets: [
    [
      path.resolve('./openraven-preset'),
      {
        docs: {
          routeBasePath: '',
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://github.com/openraven/docs/tree/master/docs',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
