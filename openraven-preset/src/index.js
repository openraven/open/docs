/**
 * Openraven modified version of classic preset
 */
const path = require('path');

module.exports = function preset(context, opts = {}) {
  const {siteConfig = {}} = context;
  const {themeConfig} = siteConfig;
  const {algolia, googleAnalytics, gtag} = themeConfig;

  const isProd = process.env.NODE_ENV === 'production';
  return {
    themes: [
      ['@docusaurus/theme-classic', opts.theme],
      [path.resolve('openraven-theme'), opts.theme],
      // Don't add this if algolia config is not defined
      algolia && '@docusaurus/theme-search-algolia',

    ],
    plugins: [
      ['@docusaurus/plugin-content-docs', opts.docs],
      ['@docusaurus/plugin-content-pages', opts.pages],
      isProd && googleAnalytics && '@docusaurus/plugin-google-analytics',
      isProd && gtag && '@docusaurus/plugin-google-gtag',
      isProd && ['@docusaurus/plugin-sitemap', opts.sitemap],
    ],
  };
};
