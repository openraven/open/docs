/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 * This is the docs page from the theme classic of docusaurus v2 2.0.0-alpha.40
 * with some custom logic to use the homepage react component on a certain route
 */

import React from 'react';
import {MDXProvider} from '@mdx-js/react';

import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import renderRoutes from '@docusaurus/renderRoutes';
import Layout from '@theme/Layout';
import DocSidebar from '@theme/DocSidebar';
import MDXComponents from '@theme/MDXComponents';
// import NotFound from '@theme/NotFound';
import {matchPath} from '@docusaurus/router';

import styles from './styles.module.css';
import HomePage from '../../../../src/home-page/home-page';
import { routeConstant as homepageRoute } from '../../../../src/home-page/constants';

const DOC_MENU_WIDTH = '300px';
function matchingRouteExist(routes, pathname) {
  return routes.some(route => matchPath(pathname, route));
}

function DocPage(props) {
  const {route, docsMetadata, location} = props;
  const {permalinkToSidebar, docsSidebars, version} = docsMetadata;
  const sidebar = permalinkToSidebar[location.pathname.replace(/\/$/, '')];
  const {siteConfig: {themeConfig = {}} = {}} = useDocusaurusContext();
  const {sidebarCollapsible = true} = themeConfig;

  if (!matchingRouteExist(route.routes, location.pathname) && location.pathname !== homepageRoute) {
    return (
      <Layout>
        <h1>Page Not Found</h1>
      </Layout>
    );
  }
  return (
    <Layout version={version}>
      <div className={styles.docPage}>
        {sidebar && (
          <div className={styles.docSidebarContainer}>
            <DocSidebar
              docsSidebars={docsSidebars}
              location={location}
              sidebar={sidebar}
              sidebarCollapsible={sidebarCollapsible}
            />
          </div>
        )}
        {
          matchPath(location.pathname, homepageRoute) ? (
            <>
              <HomePage />
              <div style={{ width: DOC_MENU_WIDTH }} />
            </>
          ) : (
          <MDXProvider components={MDXComponents}>
            <main className={styles.docMainContainer}>
              {renderRoutes(route.routes)}
            </main>
          </MDXProvider>
          )
        }
      </div>
    </Layout>
  );
}

export default DocPage;
