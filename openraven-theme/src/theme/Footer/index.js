import React, { lazy, Suspense } from 'react';
import { IntlProvider } from 'react-intl';
import { CORP_SITE_URL } from '../../constants';

const Fallback = <div />;
// Use a mock router implementation because docusaurus' router context is not provided
// to their navbar components
function Footer() {
  if (typeof window === 'undefined') {
    return Fallback;
  }

  const SharedFooter = lazy(() => import('./footer-wrapper'));

  return (
    <Suspense fallback={Fallback}>
      <IntlProvider>
        <SharedFooter
          isCorpSite={false}
          corpWebsiteUrl={CORP_SITE_URL}
        />
      </IntlProvider>
    </Suspense>
  );
}

export default Footer;
