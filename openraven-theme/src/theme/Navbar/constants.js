import { BASE_NAVIGATION_CONFIG } from '@open-raven/react-styleguide';
import { CORP_SITE_URL } from '../../constants';

export const navigationConfig = BASE_NAVIGATION_CONFIG.map((item) => ({
  ...item,
  links: item.links.map((link) => (link.key === 'docs' ? {
    ...link,
    to: '/',
  } : {
    ...link,
    external: true,
    to: `${CORP_SITE_URL}/${link.key}`,
  })),
}));
