import React from 'react';
import { Header } from '@open-raven/react-styleguide';
import { navigationConfig } from './constants';
import { CORP_SITE_URL } from '../../constants';

const HeaderWrapper = () => (
  <Header
    hideSearch
    homePageUrl={CORP_SITE_URL}
    navigationConfig={navigationConfig}
  />
);

export default HeaderWrapper;
