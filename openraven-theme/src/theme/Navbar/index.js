import React, { lazy, Suspense } from 'react';
import { IntlProvider } from 'react-intl';
import { Router } from 'react-router';
import { createBrowserHistory } from 'history';

const Fallback = <div />;

// Use a mock router implementation because docusaurus' router context is not provided
// to their navbar components
function Navbar() {
  if (typeof window === 'undefined') {
    return Fallback;
  }

  const mockHistory = createBrowserHistory();
  const HeaderWrapper = lazy(() => {
    return import("./header-wrapper");
  });

  return (
    <Router history={mockHistory}>
      <Suspense fallback={Fallback}>
        <IntlProvider>
          <HeaderWrapper />
        </IntlProvider>
      </Suspense>
    </Router>
  );
}

export default Navbar;
