/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

module.exports = {
  someSidebar: [
    {
      type: 'doc',
      id: 'home',
    },
    {
      type: 'category',
      label: 'Overview',
      items: ['the-platform', 'the-architecture'],
    },
    {
      type: 'category',
      label: 'Installation',
      items: ['before-you-install', 'installation-walkthrough'],
    },
    {
      type: 'category',
      label: 'User Guide',
      items: ['navigation', 'landing-menu', 'explorer-menu', 'asset-groups-menu', 'reports-menu', 'settings-menu'],
    },
    {
      type: 'category',
      label: 'Getting Help',
      items: ['reporting-bugs' ,'improving-docs' , 'request-features' ],
    },
    {
      type: 'category',
      label: 'Developer Guide',
      items: ['developer-guide-coming-soon' ],
    },
  ],
};
