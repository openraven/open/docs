import React from 'react';
import useBaseUrl from '@docusaurus/useBaseUrl'; // Add to the top of the file below the front matter.
import styles from './styles.module.css';

export const PdfLink = ({
  url,
  alt,
  fileSize
}) => (
  <p className={styles['pdf-link']}>
    <a href={useBaseUrl(url)}>
      Click to View
    </a>
    <img
      className={styles['pdf-link__img']}
      src={useBaseUrl('img/pdf.svg')}
      alt={alt}
    />
    {fileSize}
  </p>
);
