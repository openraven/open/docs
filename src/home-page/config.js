export const quickLinks = [
  {
    title: 'Overview',
    imageUrl: 'img/overview.svg',
    link: 'the-platform',
  },
  {
    title: 'Installation',
    imageUrl: 'img/installation.svg',
    link: 'before-you-install',
  },
  {
    title: 'User Guide',
    imageUrl: 'img/user-guide.svg',
    link: 'navigation',
  },
  {
    title: 'Getting Help',
    imageUrl: 'img/configuration.svg',
    link: 'reporting-bugs',
  },
  {
    title: 'Developer Guide',
    imageUrl: 'img/developer-guide.svg',
    link: 'open-source-code',
  },
  {
    title: 'Whitepapers',
    imageUrl: 'img/whitepapers.svg',
    link: 'white-papers',
  },
];

export const popularDocs = [
  {
    title: 'AWS IAM Role for Installation',
    link: 'popular-1'
  },
  {
    title: 'Adding an AWS Discovery Account',
    link: 'popular-2'
  },
  {
    title: 'DMAP Scanning in AWS',
    link: 'popular-3'
  },
  {
    title: 'Estimating Your Hosting Costs',
    link: 'popular-4'
  },
  {
    title: 'DMAP Scanning your Network',
    link: 'popular-5'
  },
  {
    title: 'Setup Shadow Account Monitor',
    link: 'popular-6'
  },
];
