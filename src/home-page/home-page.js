import React, { useState } from 'react';
import classnames from 'classnames';
import Link from '@docusaurus/Link';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';
import { quickLinks, popularDocs } from './config';
import SearchBar from './HomePageSearchBar';

function QuickLink({
  imageUrl,
  title,
  link,
  imageWidth,
  imageHeight,
}) {
  const imgUrl = useBaseUrl(imageUrl);
  const linkUrl = useBaseUrl(link);

  return (
    <Link
      className={classnames('col col--4', styles.quickLink)}
      to={linkUrl}
    >
      {imgUrl && (
        <img
          className={styles['quickLink__feature-image']}
          src={imgUrl}
          alt={title}
          style={{
            width: imageWidth,
            height: imageHeight,
          }}
        />
      )}
      <p className={styles['quickLink__title']}>{title}</p>
    </Link>
  );
}

function PopularDoc({
  title,
  link,
}) {
  const imgUrl = useBaseUrl('img/popular-doc.svg');
  const linkUrl = useBaseUrl(link);

  return (
    <Link className={styles['popular-doc']} to={linkUrl}>
      <img
        className={styles['popular-doc__image']}
        src={imgUrl}
        alt={`popular doc: ${title}`}
      />
      <p className={styles['popular-doc__title']}>{title}</p>
    </Link>
  );
}

function Home() {
  const [isSearchBarExpanded, handleSearchBarToggle] = useState(false);

  return (
    <div className={styles['home-page']}>
      <main>
        {
          quickLinks && quickLinks.length && (
          <section className={styles.quickLinks}>
            <div className={styles['quickLinks__container']}>
              {quickLinks.map((props, idx) => (
                <QuickLink key={idx} {...props} />
              ))}
            </div>
          </section>
          )
        }
        <SearchBar
          isSearchBarExpanded={isSearchBarExpanded}
          handleSearchBarToggle={handleSearchBarToggle}
        />
        {
          popularDocs && popularDocs.length && (
          <section className={styles.popularDocs}>
            <p className={classnames(styles['popular-docs__title'], styles['home__subheading'])}>
              Popular Docs
            </p>
            <div className={styles['popular-docs__container']}>
              {popularDocs.map((props, idx) => (
                <PopularDoc key={idx} {...props} />
              ))}
            </div>
          </section>
          )
        }
      </main>
    </div>
  );
}

export default Home;
