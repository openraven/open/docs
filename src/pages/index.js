import React from 'react';
import { Redirect } from '@docusaurus/router';
import { routeConstant } from '../home-page/constants';

// Force docs to be the home page
function Home() {
  return (
    <Redirect to={routeConstant} />
  );
}

export default Home;
